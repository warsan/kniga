![Состояние правок](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

Пример веб-сайта [GitBook] с использованием GitLab Pages.

Узнайте больше о страницах GitLab на https://pages.gitlab.io 
и в официальной документации https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- НЕ РЕДАКТИРУЙТЕ ЭТОТ РАЗДЕЛ, ВМЕСТО ЭТОГО ПЕРЕЗАПУСТИТЕ doctoc ЧТОБЫ ОБНОВИТЬ -->
**Table of Contents**  *генерируется с помощью [DocToc](https://github.com/thlorenz/doctoc)*

<!-- НАЧАЛО генерации документа TOC, оставьте коммент для автообновления -->
<!-- START doctoc -->
- [GitLab CI](#gitlab-ci)
- [Правка на местном уровне](#правка-на-местном-уровне)
- [Страницы пользователя или группы GitLab](#страницы-пользователя-или-группы-gitlab)
- [Вы разветвили этот проект?](#вы-разветвили-этот-проект)
- [Устранение неполадок](#устранение-неполадок)
<!-- END doctoc -->
<!-- КОНЕЦ генерации оглавления, для автообновления оставьте коммент здесь -->

## GitLab CI

Статические страницы этого проекта создаются [GitLab CI][ci] в соответствии с шагами, 
определёнными в [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
# требуется среда NodeJS 10
image: node:10

# добавить node_modules в кеш для ускорения сборки
cache:
  paths:
    - node_modules/ # Модули и зависимости Node

before_script:
  - npm install gitbook-cli -g # установить gitbook
  - gitbook fetch 3.2.3 # получить окончательную стабильную версию
  - gitbook install # добавьте все требуемые плагины в book.json

test:
  stage: test
  script:
    - gitbook build . public # построить на общественном пути
  only:
    - branches # эта работа затронет все ветви, кроме 'master'
  except:
    - master
    
# задание 'pages' развернет и создаст ваш сайт на "публичном" пути
pages:
  stage: deploy
  script:
    - gitbook build . public # построить на общественном пути
  artifacts:
    paths:
      - public
    expire_in: 1 week
  only:
    - master # эта работа повлияет только на ветвь 'master'
```

## Правка на местном уровне

Для локальной работы с этим проектом вам необходимо выполнить следующие шаги:

1. Форк, клон или загрузка этого проекта
1. [Install][] GitBook `npm install gitbook-cli -g`
1. Получение последней стабильной версии GitBook `gitbook fetch latest`
1. Предварительный просмотр проекта: `gitbook serve`
1. Добавить содержание
1. Создайте веб-сайт: `gitbook build` (optional)
1. Внесите свои изменения в ветку master: `git push`

Подробнее на сайте GitBook [documentation][].

## Страницы пользователя или группы GitLab

Чтобы использовать этот проект в качестве веб-сайта пользователя/группы, вам понадобится еще один
шаг: просто переименуйте свой проект в `namespace.gitlab.io`, где `namespace` - это
ваше `имя пользователя` или `имя группы`. Это можно сделать, перейдя в раздел
**Настройки** проекта.

Подробнее о [Страницы пользователя/группы][userpages] и [Страницы проекта][projpages].

## Вы разветвили этот проект?

Если вы форкнули этот проект для собственного использования, пожалуйста, перейдите в его
**Настройки** и удалите ветви, которые вам не понадобятся.  
*если только вы не хотите внести свой вклад в проект upstream.*

## Устранение неполадок

1. CSS отсутствует! Это означает две вещи:

    Либо вы неправильно задали URL CSS в ваших шаблонах, либо
    ваш статический генератор имеет параметр конфигурации, который должен быть явным образом
    задан для обслуживания статических активов по относительному URL.

----

Forked from @virtuacreative

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
